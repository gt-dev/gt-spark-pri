# -*- coding: UTF-8 -*-
import sys
import os
import time
from datetime import datetime

from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

from job.captcha_switch import captcha_switch
from job.captcha_switch_ua import captcha_switch_ua
from job.captcha import captcha
from job.hash import accesscontrol_hash
from job.cluster import accesscontrol_cluster

from utils.tools import json_decode
from config import SPARK_MASTER, APP_NAME, \
    TOPICS, ZOOKEEPER, KAFKA_GROUP, CHECKPOINT_DIR, AWAIT_TIME

sys.path.append(os.path.abspath(os.path.dirname(__file__)))


def init_log_streaming():
    sc = SparkContext(SPARK_MASTER, APP_NAME)
    ssc = StreamingContext(sc, AWAIT_TIME)
    ssc.checkpoint(CHECKPOINT_DIR)
    kvs = KafkaUtils.createStream(ssc, ZOOKEEPER, KAFKA_GROUP, {TOPICS: 3})
    lines = kvs.map(lambda x: x[1])
    logs = lines.map(json_decode).filter(lambda x: x is not None)
    logs.cache()
    return ssc, logs

def wait():
    wait_time = AWAIT_TIME - (int(time.time()) % AWAIT_TIME)
    print('Waiting to start streaming job, need to wait: %sseconds' % (wait_time))
    time.sleep(wait_time)
    now = datetime.now()
    print('Start streaming job at: %s' % (now))

def process():
    start_time = time.time()

    wait()

    ssc, logs = init_log_streaming()

    captcha_switch.run(logs)
    captcha_switch_ua.run(logs)
    captcha.run(logs)
    accesscontrol_hash.run(logs)
    accesscontrol_cluster.run(logs)

    ssc.start()
    ssc.awaitTermination()

    end_time = time.time()

    print('Time cost: %s' % (end_time-start_time))


if __name__ == '__main__':
    process()

