import numpy as np
import math

class Flow(object):

    def __init__(self, l, flow_d, step_size):
        self.l = l
        self.flow_d = flow_d
        self.step_size = step_size

    def continuity_judgment(self, flow_list, p):
        f_len = len(flow_list)
        prop = []
        for i in range(f_len-1):
            proportion = float(flow_list[i+1])/flow_list[i]
            prop.append(proportion)

        if any(map(lambda l: l >= p[0], prop)) or any(map(lambda l: l <= p[1], prop)):
            return 'suspect_risk'
        else:
            return 'safe_now'

    def evenly_distributed(self):
        l_len = len(self.l)
        if l_len <= self.step_size:
            result = self.num_count0(self.l, self.flow_d)
            return result
        else:
            l0 = []
            for j in range(l_len - self.step_size + 1):
                l0.append(list(np.arange(j, j + self.step_size)))
            result = []
            for p in l0:
                pp = [self.l[x0] for x0 in range(p[0], p[-1] + 1)]
                result.append(self.num_count1(pp, self.flow_d))
            return result

    def result(self, empty_l, data_max, para):
        if data_max <= para["para0"][0]:
            # return self.continuity_judgment(empty_l, para["para0"][1])
            return 'safe_now'
        elif para["para0"][0] < data_max <= para["para1"][0]:
            return self.continuity_judgment(empty_l, para["para0"][1])
        elif para["para1"][0] < data_max <= para["para2"][0]:
            return self.continuity_judgment(empty_l, para["para1"][1])
        elif para["para2"][0] < data_max <= para["para3"][0]:
            return self.continuity_judgment(empty_l, para["para2"][1])
        else:
            return self.continuity_judgment(empty_l, para["para3"][1])

    def num_count0(self, empty_l, para):
        l_len = len(empty_l)
        if l_len in [0, 1]:
            return 'safe_now'
        elif l_len >= 2:
            return self.result(empty_l, max(empty_l), para)

    def num_count1(self, empty_l, para):
        new_l = sorted(empty_l)
        l_len = len(new_l)
        l0 = new_l[int(math.ceil(l_len/5)):int(math.ceil(l_len/5)*4)]
        l_min, l_max = l0[0], l0[-1]
        return self.result(empty_l, l_max, para)
