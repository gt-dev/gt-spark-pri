import json
import time

def json_decode(line):
    try:
        return json.loads(line)
    except:
        return None

def get_prefix():
    n_time = time.localtime(time.time() - 3600)
    prefix = time.strftime("%Y%m%d-3600-", n_time)
    prefix += str(int(time.strftime("%H", n_time)))
    return prefix