# -*- coding: UTF-8 -*-
import datetime
import pymysql
from copy import deepcopy

from config import MYSQL_DB, MYSQL_PASSWORD, MYSQL_USER, MYSQL_HOST

COUNT_KEY = ("register", "ajax", "success", "fail", "forbidden", "get", "total", "success_click",
             "success_slide", "success_fullpage", "click", "slide", )

class CaptchaDimension():

    DIMENSION = 'CaptchaDimension'
    RECORD = {
        'register': 0, 'get': 0, 'ajax': 0, 'total': 0, 'success': 0, 'fail': 0, 'forbidden': 0,
        'success_click': 0, 'success_slide': 0, 'success_fullpage': 0, 'click': 0, 'slide': 0
    }
    WINDOW_LENGTH = 60 * 10
    SLIDE_INTERVAL = 60 * 10
    #WINDOW_LENGTH = 10
    #SLIDE_INTERVAL = 10

    def log_filter(self, log):
        method = log.get('method', 'none')
        method = method.lower()

        if method not in ("get", "ajax", "register", "validate"):
            return False

        if not log.get('captcha_id', None):
            return False

        if not log.get("challenge_type", ""):
            return False

        return True

    def log_2_record(self, log):
        record = deepcopy(self.RECORD)
        method = log['method'].lower()

        if method != 'validate':
            record[method] += 1

        if method == "ajax":
            challenge_type = log.get("challenge_type", "")
            if challenge_type == "fullpage":
                record["total"] += 1
            else:
                record[challenge_type] += 1

            result = log.get("result", "")

            if result:
                record[result] += 1

            if result == "success":
                record["success_%s" % challenge_type] += 1

        return record

    def merge(self, record1, record2):
        record = deepcopy(record1)
        for key in list(record.keys()):
            record[key] = record1[key] + record2[key]
        return record

    def inverse_merge(self, record1, record2):
        record = deepcopy(record1)
        for key in list(record.keys()):
            record[key] = record1[key] - record2[key]
        return record

    def save_db(self, partition):
        conn = pymysql.connect(host=MYSQL_HOST, user=MYSQL_USER, password=MYSQL_PASSWORD, db=MYSQL_DB, charset="utf8")
        with conn.cursor() as cursor:
            sql = "INSERT INTO `Statistics` (`register`, `ajax`, `success`, `fail`, `forbidden`, `get`, `total`, " \
                "`success_click`, `success_slide`, `success_fullpage`, `click`, `slide`, `captcha_id`, `wtime`) VALUES " \
                "(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, '%s');"
        
            for captcha_id, data in partition:
                cursor.execute("SELECT id FROM private_captcha WHERE gt_id='%s';" % captcha_id)
                result = cursor.fetchone()
                if result:
                    args = [data[k] for k in COUNT_KEY]
                    args.append(result[0])
                    args.append(datetime.datetime.now())
                    sql = sql % tuple(args)
                    cursor.execute(sql)
        
            conn.commit()

    def add_key(self, log):
        return (log['captcha_id'], log)

    def run(self, logs):
        filt_logs = logs.filter(self.log_filter)
        kv_logs = filt_logs.map(self.add_key)
        records = kv_logs.mapValues(self.log_2_record)
        records_merge = records.reduceByKeyAndWindow(self.merge, self.inverse_merge,
                                                     self.WINDOW_LENGTH, self.SLIDE_INTERVAL)

        records_merge.foreachRDD(lambda t, rdd: rdd.foreachPartition(self.save_db))

captcha = CaptchaDimension()
