# -*- coding: UTF-8 -*-
import redis
import time
from copy import deepcopy

from utils.tools import get_prefix
from utils.flow import Flow
from config import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD, REDIS_DB_CACHE

class CaptchaSwitchDimension():

    DIMENSION = 'CaptchaSwitchDimension'
    FLOW_D = {
        "para0": [20, (10., 1./10)],
        "para1": [200, (5., 1./5)],
        "para2": [2000, (3., 1./3)],
        "para3": [5000, (2., 0.5)]
    }
    WINDOW_LENGTH = 300
    SLIDE_INTERVAL = 60
    #WINDOW_LENGTH = 10
    #SLIDE_INTERVAL = 10

    # pf_count: passtime_forbidden_count
    # f_count: forbidden_count
    # rt_count: request_time_count
    RECORD = {'total': 0, 'pf_count': 0, 'f_count': 0, 'rt_count': {}}

    def log_filter(self, log):
        method = log.get('method', 'none')
        method = method.lower()

        if method not in ('ajax',):
            return False

        if not log.get('captcha_id'):
            return False

        return True

    def add_key_2_log(self, log):
        prefix = get_prefix()
        key = '%s:%s-%s' % (self.DIMENSION, prefix, log['captcha_id'])
        return (key, log)

    def log_2_record(self, log):
        record = deepcopy(self.RECORD)
        record['total'] += 1

        if log.get('forbidden', None):
            record['f_count'] += 1
            forbidden_reason = log.get('forbidden')
            if 'passtime forbidden' in forbidden_reason:
                record['pf_count'] += 1

        rt_minute = int(time.time()/60)
        record['rt_count'][rt_minute] = 1

        return record

    def merge(self, record1, record2):
        record = deepcopy(record1)
        for key in list(record.keys()):
            if key != 'rt_count':
                record[key] = record1[key] + record2[key]
            else:
                for s_key, s_value in record2['rt_count'].items():
                    record['rt_count'][s_key] = record['rt_count'].get(s_key, 0) + s_value

        return record

    def inverse_merge(self, record1, record2):
        record = deepcopy(record1)
        now = int(time.time()/60)

        for key in list(record.keys()):
            if key != 'rt_count':
                record[key] = record1[key] - record2[key]
            else:
                for s_key in list(record['rt_count'].keys()):
                    if s_key < (now - 5):
                        # delete 5min ago
                        record['rt_count'].pop(s_key)

        return record

    def judege_risk(self, kv_record):
        key, record = kv_record
        captcha_id = key.split('-')[-1]

        try:
            total = record.get('total')
            f = record.get('f_count', 0)
            pf = record.get('pf_count', 0)
            rt = record.get('rt_count', {})

            res = {
                'request_time_factor': self.request_time_rule(rt, 5)
            }
            if 'suspect_risk' in res.values():
                effect_time = 1800
            else:
                effect_time = 0
        except:
            return captcha_id, 0

        return captcha_id, effect_time

    def passtime_rule(self, total, pf_count, pf_threshold):
        if total == 0 or pf_count <= 100:
            return self.result(0, 1)
        else:
            return self.result(float(pf_count)/total, pf_threshold)

    def forbidden_rate_rule(self, total, f_count, f_threshold=0.4):
        if total == 0 or f_count <= 200:
            return self.result(0, 1)
        else:
            forbidden_rate = float(f_count) / total
            return self.result(forbidden_rate, f_threshold)

    def request_time_rule(self, rt_count, step):
        if len(rt_count) < 5:
            return 'safe_now'
        rt0 = sorted(rt_count.items(), key=lambda l: l[0], reverse=False)
        rt = [x[1] for x in rt0]
        flow = Flow(rt, self.FLOW_D, step)
        res = flow.evenly_distributed()
        if 'suspect_risk' in res:
            return 'suspect_risk'
        else:
            return 'safe_now'

    def result(self, a, b):
        return 'suspect_risk' if a >= b else 'safe_now'

    def save_2_redis(self, data):
        redis_client = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB_CACHE, password=REDIS_PASSWORD)
      
        if not data:
            return

        for item in data:
            k, v, expire = item
            redis_client.set(k, v, expire)

    def save_db(self, partition):
        points_list = []
        points, count = [], 0

        for key, value in partition:
            points.append(('slide_2_click:%s' % key, 'null', value))
            count += 1
            if count >= 100:
                points_list.append(points)
                points, count = [], 0

        if points:
            points_list.append(points)

        self.save_2_redis(points_list)

    def run(self, logs):
        filt_logs = logs.filter(self.log_filter)
        key_logs = filt_logs.map(self.add_key_2_log)
        records = key_logs.mapValues(self.log_2_record)
        records_merge = records.reduceByKeyAndWindow(self.merge, self.inverse_merge,
                                                     self.WINDOW_LENGTH, self.SLIDE_INTERVAL)

        risk_captcha = records_merge.map(self.judege_risk).filter(lambda x: x[1] > 0)
        risk_captcha.foreachRDD(lambda rdd: rdd.foreachPartition(self.save_db))


captcha_switch = CaptchaSwitchDimension()
