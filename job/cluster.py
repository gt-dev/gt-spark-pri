#!/usr/bin/env python
# -*- coding:utf-8 -*-
import redis
import traceback
from copy import deepcopy
import numpy as np

from config import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD, REDIS_DB_CACHE

class AccesscontrolClusterDimension():

    RECORD = np.zeros((16, 50), np.int).tolist()
    WINDOW_LENGTH = 60 * 60 * 2
    SLIDE_INTERVAL = 600
    #WINDOW_LENGTH = 10
    #SLIDE_INTERVAL = 10
    EXPIRE = 86400

    def log_2_record(self, log):
        record = deepcopy(self.RECORD)
        try:
            req = np.array(log["feature_list"])
            for i in range(len(req)):
                record[i][req[i]] = 1
        except:
            traceback.print_exc()
        finally:
            return record

    def merge(self, record1, record2):
        for i in range(16):
            for j in range(50):
                record1[i][j] += record2[i][j]
        return record1

    def inverse_merge(self, record1, record2):
        for i in range(16):
            for j in range(50):
                record1[i][j] -= record2[i][j]
        return record1

    def build_result(self, key, value):
        result_value = {
            "_id": '-'.join(["Cluster", key]),
            "denses": value
        }
        return result_value

    def save_2_redis(self, data):
        redis_client = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB_CACHE, password=REDIS_PASSWORD)

        if not data:
            return

        for item in data:
            k, v, expire = item
            redis_client.set(k, v, expire)

    def save_db(self, partition):
        points_list = []
        points, count = [], 0

        for key, value in partition:
            try:
                if sum(value[0]) > 400:
                    points.append(['-'.join(["Cluster", key]), self.build_result(key, value), self.EXPIRE])
                    count += 1

                if count >= 100:
                    points_list.append(points)
                    points, count = [], 0
            except:
                traceback.print_exc()

        if points:
            points_list.append(points)
        self.save_2_redis(points_list)

    def log_filter(self, log):
        method = log.get('method', 'none')
        method = method.lower()

        if method not in ('ajax',):
            return False

        if not log.get('captcha_id', None) or not log.get('feature_list', None):
            return False

        return True

    def add_key(self, log):
        return (log['captcha_id'], log)

    def run(self, logs):
        filt_logs = logs.filter(self.log_filter)
        kv_logs = filt_logs.map(self.add_key)
        records = kv_logs.mapValues(self.log_2_record)
        records_merge = records.reduceByKeyAndWindow(self.merge, None, self.WINDOW_LENGTH, self.SLIDE_INTERVAL)
        records_merge.foreachRDD(lambda t, rdd: rdd.foreachPartition(self.save_db))


accesscontrol_cluster = AccesscontrolClusterDimension()
