# -*- coding: UTF-8 -*-
import hashlib
import numpy as np
from copy import deepcopy

import redis

from utils.tools import get_prefix
from config import REDIS_HOST, REDIS_PORT, REDIS_PASSWORD, REDIS_DB_CACHE

class CaptchaSwitchUADimension():

    DIMENSION = 'CaptchaSwitchUADimension'
    RECORD = {'total': 0}
    WINDOW_LENGTH = 1800
    SLIDE_INTERVAL = 300
    #WINDOW_LENGTH = 10
    #SLIDE_INTERVAL = 10

    def log_filter(self, log):
        method = log.get('method', 'none')
        method = method.lower()

        if method not in ('ajax'):
            return False

        if not log.get('captcha_id'):
            return False

        return True

    def add_key_2_log(self, log):
        prefix = get_prefix()
        key = '%s:%s-%s' % (self.DIMENSION, prefix, log['captcha_id'])
        return (key, log)

    def log_2_record(self, log):
        record = deepcopy(self.RECORD)
        record['total'] += 1
        ua = log.get('UA') or 'none'
        ua_md5 = hashlib.md5(ua.encode('utf-8')).hexdigest()
        record[ua_md5] = record.get(ua_md5, 0) + 1
        return record

    def merge(self, record1, record2):
        record = deepcopy(record1)
        for key in list(record2.keys()):
            record[key] = record.get(key, 0) + record2[key]
        return record

    def inverse_merge(self, record1, record2):
        record = deepcopy(record1)
        for key in list(record2.keys()):
            record[key] = record.get(key, 0) - record2[key]
        return record

    def judege_risk(self, kv_record):
        try:
            key, record = kv_record
            captcha_id = key.split('-')[-1]
            total = record.pop('total')
            result = self.UA_rule(total, record)
            effect_time = 300 if result == 'suspect_risk' else 0
        except:
            return captcha_id, 0

        return captcha_id, effect_time

    def UA_rule(self, total, ua_count):
        ua_list = np.array(ua_count.values())
        if total <= 1000:
            return 'safe_now'
        else:
            return self.cal_peak(ua_list)

    def result(self, a, b):
        return 'suspect_risk' if a >= b else 'safe_now'

    def cal_peak(self, l):
        s = np.std(l)
        diff = ((l - np.mean(l)) ** 4).sum()
        res = float(diff) / ((len(l) - 1) * s ** 4)
        return self.result(5, res)

    def save_2_redis(self, data):
        redis_client = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=REDIS_DB_CACHE, password=REDIS_PASSWORD)

        if not data:
            return

        for item in data:
            k, v, expire = item
            redis_client.set(k, v, expire)

    def save_db(self, partition):
        points_list = []
        points, count = [], 0

        for key, value in partition:
            points.append(('slide_2_click:%s' % key, 'null', value))
            count += 1
            if count >= 100:
                points_list.append(points)
                points, count = [], 0

        if points:
            points_list.append(points)
        self.save_2_redis(points_list)

    def run(self, logs):
        filt_logs = logs.filter(self.log_filter)
        key_logs = filt_logs.map(self.add_key_2_log)
        records = key_logs.mapValues(self.log_2_record)
        records_merge = records.reduceByKeyAndWindow(self.merge, self.inverse_merge,
                                                     self.WINDOW_LENGTH, self.SLIDE_INTERVAL)
        risk_captcha = records_merge.map(self.judege_risk).filter(lambda x: x[1] > 0)
        risk_captcha.foreachRDD(lambda rdd: rdd.foreachPartition(self.save_db))


captcha_switch_ua = CaptchaSwitchUADimension()
